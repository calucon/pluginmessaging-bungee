package de.xxlcreeper.io;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DirectoryWatcher {

	private File dir;
	private HashMap<String, Long> content;
	private ArrayList<DirectoryChangeListener> listener = null;
	private Thread thread = null;
	private boolean x = true;
	private boolean create = false;
	private boolean modify = false;
	private boolean delete = false;
	
	/**
	 * 
	 * @param directory The directory to listen to
	 * 		It automatically listens to all 3 different "Event"-types
	 */
	public DirectoryWatcher(File directory){		
		this(directory, WatcherKey.Create, WatcherKey.Modify, WatcherKey.Delete);
	}
	
	/**
	 * 
	 * @param directory The directory to listen to
	 * @param keys All "Events" you want to get notified about
	 */
	public DirectoryWatcher(File directory, WatcherKey... keys){
		if(!directory.isDirectory()) throw new IllegalArgumentException("The File must be a Directory!");
		if(!directory.exists()) throw new IllegalArgumentException("The Directory does not exist!");

		for(WatcherKey key : keys){
			if(key == WatcherKey.Create) this.create = true;
			if(key == WatcherKey.Modify) this.modify = true;
			if(key == WatcherKey.Delete) this.delete = true;
		}
		
		this.dir = directory;
		this.content = new HashMap<>(currentFiles());
	}
	
//------------------------------------------	
	
	/**
	 * 
	 * @return A List containing all changes in the selected Directory since the last time the method has been called
	 */
	public synchronized List<WatchResult> getChanges(){
		HashMap<String, Long> update = new HashMap<>(currentFiles());
		ArrayList<WatchResult> changes = new ArrayList<>();
		ArrayList<String> arr = new ArrayList<>();
		
		//Check for modified and new Files
		for(Entry<String, Long> e : content.entrySet()){
			if(update.containsKey(e.getKey())){
				if(!modify) continue;
				if(update.get(e.getKey()) > e.getValue()){
					File f = new File(dir, e.getKey());
					changes.add(new WatchResult(f, WatcherKey.Modify));
					arr.add(e.getKey());
				}
			} else {
				if(!delete) continue;
				File f = new File(dir, e.getKey());
				changes.add(new WatchResult(f, WatcherKey.Delete));
			}
		}
		
		//Check for created Files
		for(String str : update.keySet()){
			if(!create) break;
			if(!arr.contains(str) && !content.keySet().contains(str)){
				File f = new File(dir, str);
				changes.add(new WatchResult(f, WatcherKey.Create));
			}
		}
		
		//Update content map
		content.clear();
		content.putAll(update);
		
		return changes;
	}
	
	
//------------------------------------------
	
	/**
	 * 
	 * @param listener
	 * 		new Instance of the {@link DirectoryChangeListener} class or a class that extends the {@link DirectoryChangeListener}
	 */
	public void addChangeListner(DirectoryChangeListener listener){
		if(this.listener == null) this.listener = new ArrayList<>();
		this.listener.add(listener);
		if(thread == null || !thread.isAlive()){
			thread = new Thread(this.getClass().getSimpleName()){
				@Override
				public void run(){
					while(x){
						for(WatchResult res : getChanges()){
							//new ArrayList to prevent a ConcurrentModificationException
							for(DirectoryChangeListener l : new ArrayList<>(DirectoryWatcher.this.listener)){
								l.changed(res);
							}
						}
						try{Thread.sleep(1);}catch(InterruptedException e){}	
					}
				}
			};
			thread.start();
			//Stopping thread at Runtime exit
			Runtime.getRuntime().addShutdownHook(new Thread(){
				@Override
				public void run(){
					x = false;
				}
			});
		}
	}
	
	/**
	 * 
	 * @param listener
	 * 		removes the {@link DirectoryChangeListener} from the Event queue
	 */
	public void removeChangeListener(DirectoryChangeListener listener){
		this.listener.remove(listener);
	}
	
	/**
	 * Stops all instances of {@link DirectoryChangeListener}
	 */
	public void stopChangeListenerThread(){
		this.x = false;
		this.thread = null;
	}
	
//------------------------------------------
	
	//Returns an updated list of all Files in the the Directory
	private Map<String, Long> currentFiles(){
		HashMap<String, Long> map = new HashMap<>();
		for(File f : dir.listFiles()){
			map.put(f.getName(), f.lastModified());
		}
		return map;
	}
	
}
