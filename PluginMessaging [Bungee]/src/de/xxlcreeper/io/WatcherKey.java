package de.xxlcreeper.io;

public enum WatcherKey {

	Create,
	Modify,
	Delete;

}