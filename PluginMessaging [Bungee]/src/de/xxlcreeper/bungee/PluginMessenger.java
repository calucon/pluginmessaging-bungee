package de.xxlcreeper.bungee;

import net.md_5.bungee.api.plugin.Plugin;

public class PluginMessenger extends Plugin{

	de.xxlcreeper.bungee.pluginmessaging.fs.Main fs;
	
	@Override
	public void onEnable() {
		new de.xxlcreeper.bungee.pluginmessaging.pmc.Main(this);
		fs = new de.xxlcreeper.bungee.pluginmessaging.fs.Main(this);
	}

	@Override
	public void onDisable() {
		fs.disable();
	}
	
}
