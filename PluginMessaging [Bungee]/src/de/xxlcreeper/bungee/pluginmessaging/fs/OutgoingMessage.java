package de.xxlcreeper.bungee.pluginmessaging.fs;

import java.io.File;
import java.util.Collection;

public class OutgoingMessage {

	protected File f;
	protected File f_true;
	protected Collection<? extends String> messages;
	
	protected OutgoingMessage(File f, Collection<? extends String> messages){
		this.f = f;
		this.f_true = new File(f.getParent(), f.getName().replace(Main.PREFIX_NOT_READY, ""));
		this.messages = messages;
	}
	
}
