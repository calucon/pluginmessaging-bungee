package de.xxlcreeper.bungee.pluginmessaging.fs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Random;

public class PluginMessageSender{

	private static HashMap<OutgoingMessage, Integer> loop = new HashMap<>();
	private static Random r = new Random();
	protected Thread thread;
	
	protected PluginMessageSender() {
		thread = new Thread(PluginMessageSender.class.getSimpleName()){
			@Override
			public void run(){
				try {
					while(Main.running){
						runThread();
						Thread.sleep(10);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
	}
	
//------------------------------------------
	
	/**
	 * 
	 * @param message The message
	 * @param channel The channel in which the message will be sent
	 * @param server The receiver of the message
	 */
	public static void sendMessage(String message, String channel, Server server){
		sendMessage(Arrays.asList(new String[]{message}), channel, server);
	}
	/**
	 * 
	 * @param messages List of messages
	 * @param channel The channel in which the message will be sent
	 * @param server The receiver of the message
	 */
	public static void sendMessage(Collection<? extends String> messages, String channel, Server server){
		sendMessage(messages, channel, server.getPort());
	}
	
	/**
	 * 
	 * @param message The message
	 * @param channel The channel in which the message will be sent
	 * @param destination The port of the Server who should receive the packet
	 */
	public static void sendMessage(String message, String channel, int destination){
		sendMessage(Arrays.asList(new String[]{message}), channel, destination);
	}	
	/**
	 * 
	 * @param messages List of messages
	 * @param channel The channel in which the message will be sent
	 * @param destination The port of the Server who should receive the packet
	 */
	public static void sendMessage(Collection<? extends String> messages, String channel, int destination){
		File f = new File(Main.PATH + File.separator + String.valueOf(destination), Main.PREFIX_NOT_READY + channel + identifier());
		loop.put(new OutgoingMessage(f, messages), 0);
	}
	
//------------------------------------------
	
	private void runThread(){
		try{
			for(OutgoingMessage msg : new ArrayList<>(loop.keySet())){
//				System.out.println(msg.f.exists() + " && " + msg.f_true.exists());
				if(!msg.f.exists()){
					msg.f.createNewFile();
					BufferedWriter bw = new BufferedWriter(new FileWriter(msg.f));
					for(String str : msg.messages){
						bw.write(str);
						bw.newLine();
					}
					bw.flush();
					bw.close();
					msg.f.renameTo(new File(msg.f.getParent(), msg.f.getName().replace(Main.PREFIX_NOT_READY, "")));
					loop.remove(msg);
				} else {
					if(raise(msg)){
						System.out.println("Forced Override!");
						msg.f.delete();
						msg.f_true.delete();
//						System.out.println("[PluginMessaging] Unable to send new Packet in channel '" + msg.f.getName() + "'! Is there a Server listening?");
					}
				}
			}	
		} catch(ConcurrentModificationException | IOException e){
			if(e instanceof IOException) e.printStackTrace();
		}			
	}
	
//------------------------------------------
	
	private boolean raise(OutgoingMessage msg){
		loop.put(msg, (loop.get(msg)+1));
		return (loop.get(msg) >= 1000);
	}
	
	private static String identifier(){
		return "'" + r.nextInt();
	}
	
}
