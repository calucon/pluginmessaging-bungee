package de.xxlcreeper.bungee.pluginmessaging.fs;

import java.io.File;
import java.util.regex.Pattern;

public class Server {

	private int port;
	private ServerType type;
	private String version;
	protected String filename;
	
	public Server(int port){
		this.port = port;
	}
	
	protected Server(File f) throws Exception{
		String[] arr = (this.filename = f.getName()).split(Pattern.quote("'"));
		port = Integer.parseInt(arr[0]);
		type = ServerType.fromString(arr[1]);
		version = arr[2];
	}
	
//------------------------------------------
	
	public int getPort(){
		return this.port;
	}
	
	/**
	 * 
	 * @return
	 * returns "unknown" if the Server is a BungeeCord Server
	 */
	public String getVersion(){
		return this.version;
	}
	
	public ServerType getType(){
		return this.type;
	}
	
//------------------------------------------
	
	public enum ServerType{
		Bungee,
		Bukkit;
		
		protected static ServerType fromString(String str){
			for(ServerType type : ServerType.values()){
				if(type.name().equalsIgnoreCase(str)){
					return type;
				}
			}
			return null;
		}
	}

}
