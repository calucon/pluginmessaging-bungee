package de.xxlcreeper.bungee.pluginmessaging.fs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.xxlcreeper.bungee.pluginmessaging.fs.Server.ServerType;

public class Messenger {

	protected static ArrayList<String> incomming = new ArrayList<>();
	private static File f;
	
	protected Messenger() {
		try {
			f = new File(Main.SERVER_PATH, String.valueOf(Main.port) + "'" + ServerType.Bungee + "'" + getVersion());
			f.delete();
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
//------------------------------------------
	
	/**
	 * 
	 * @param channel
	 * 		The Plugin will start to listen for messages on the selected channel
	 */
	public static void registerIncommingChannel(String channel){
		check(channel);
		if(!Messenger.incomming.contains(channel)){
			Messenger.incomming.add(channel);
		}
	}
	
	/**
	 * 
	 * @param channel
	 * The Plugin will stop listening for messages on the selected channel
	 */
	public static void unregisterIncommingChannel(String channel){
		Messenger.incomming.remove(channel);
	}
	
//------------------------------------------	
	
	/**
	 * 
	 * @param serverType Type of the Server (Bungee/Bukkit)
	 * @return List of Servers with the selected Type
	 */
	public static ArrayList<Server> getConnectedServers(ServerType serverType){
		ArrayList<Server> arr = new ArrayList<>();
		for(Server server : getConnectedServers()){
			if(server.getType() == serverType){
				arr.add(server);
			}
		}
		return arr;
	}
	
	/**
	 * 
	 * @return List of all currently connected Servers
	 */
	public static ArrayList<Server> getConnectedServers(){
		ArrayList<Server> arr = new ArrayList<>();
		for(File f : new File(Main.SERVER_PATH).listFiles()){
			try {
				arr.add(new Server(f));
			} catch (Exception e) {	}
		}
		return arr;
	}
	
//------------------------------------------	
	
	private static void check(String channel){
		if(channel.contains(File.separator) || channel.startsWith(Main.PREFIX_NOT_READY)){
			throw new IllegalArgumentException("The Channel-Name can not contain the following chars: {" + 
					File.separator + ", " + Main.PREFIX_NOT_READY + 
					"}");
		}
	}
	
	protected static void unregister(){
		f.delete();
	}
	
	private static String getVersion(){
		return "unknown";
	}
	
}
