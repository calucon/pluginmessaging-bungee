package de.xxlcreeper.bungee.pluginmessaging.fs;

import java.util.ArrayList;

public class PluginMessage {

	private String ch;
	private ArrayList<String> messages;
	
	protected PluginMessage(String channel, ArrayList<String> messages){
		this.ch = channel;
		this.messages = messages;
	}
	
//------------------------------------------
	
	/**
	 * 
	 * @return Name of the Channel the Packet was sent in
	 */
	public String getChannel(){
		return this.ch;
	}
	
	/**
	 * 
	 * @param i line
	 * @return The x. line of this PluginMessage
	 */
	public String getMessage(int i){
		return this.messages.get(i);
	}
	
	/**
	 * 
	 * @return ArrayList containing all Messages in this PluginMessage
	 */
	public ArrayList<String> getMessages(){
		return this.messages;
	}	
}
