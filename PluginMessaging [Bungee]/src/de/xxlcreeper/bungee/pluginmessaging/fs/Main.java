package de.xxlcreeper.bungee.pluginmessaging.fs;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;

import net.md_5.bungee.api.plugin.Plugin;

public class Main{
	
	protected static String PREFIX_NOT_READY = "NOT_READY_";
	protected static String CHANNEL_PATH;
	protected static String SERVER_PATH;
	protected static String PATH;
	protected static Plugin pl;
	protected static int port;
	protected static boolean running = true;
	
	public Main(Plugin pl){
		Main.pl = pl;
		port = new ArrayList<>(pl.getProxy().getConfigurationAdapter().getListeners()).get(0).getHost().getPort();
		PATH = System.getProperty("java.io.tmpdir") + "PluginMessaging";
		CHANNEL_PATH = Paths.get(PATH, String.valueOf(port)).toString();
		SERVER_PATH = Paths.get(PATH, "Serverlist").toString();
		new File(CHANNEL_PATH).mkdirs();	
		new File(SERVER_PATH).mkdirs();
		
		new Messenger();
		new PluginMessageReceiver().thread.start();
		new PluginMessageSender().thread.start();
	}
	
	/**
	 * disables the IOService
	 */
	public void disable(){
		Main.running = false;
		Messenger.unregister();
		for(File f : new File(CHANNEL_PATH).listFiles()){
			while(!f.delete());
		}
		System.gc();
	}
}
