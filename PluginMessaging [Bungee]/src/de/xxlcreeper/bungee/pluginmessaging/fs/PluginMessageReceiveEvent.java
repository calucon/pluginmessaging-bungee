package de.xxlcreeper.bungee.pluginmessaging.fs;

import java.util.ArrayList;

import net.md_5.bungee.api.plugin.Event;

public class PluginMessageReceiveEvent extends Event{

	private String ch;
	private PluginMessage msg;
	
	protected PluginMessageReceiveEvent(String channel, PluginMessage message){
		this.ch = channel;
		this.msg = message;
	}
	
//------------------------------------------	
	
	public PluginMessage message(){
		return this.msg;
	}
	
	/**
	 * 
	 * @return Name of the Channel the Packet was sent in
	 */
	public String getChannel(){
		return this.ch;
	}
	
	/**
	 * 
	 * @param i line
	 * @return The x. line of this PluginMessage
	 */
	public String getMessage(int i){
		return this.msg.getMessage(i);
	}
	
	/**
	 * 
	 * @return ArrayList containing all Messages in this PluginMessage
	 */
	public ArrayList<String> getMessages(){
		return this.msg.getMessages();
	}

}
