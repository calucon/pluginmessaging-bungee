package de.xxlcreeper.bungee.pluginmessaging.fs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import de.xxlcreeper.io.DirectoryWatcher;
import de.xxlcreeper.io.WatchResult;
import de.xxlcreeper.io.WatcherKey;

public class PluginMessageReceiver{
	
	protected Thread thread;
	private ArrayList<String> retry;
	
	protected PluginMessageReceiver() {
		retry = new ArrayList<>();
		readExistingFiles();
		
		thread = new Thread(PluginMessageReceiver.class.getSimpleName()){
			@Override
			public void run() {
				
				DirectoryWatcher w = new DirectoryWatcher(new File(Main.CHANNEL_PATH));
				
				while(Main.running){
					
					for(WatchResult res : w.getChanges()){
//						System.out.println(res.key().name() + " -> " + res.getFile().getName());
						if(res.getKey() == WatcherKey.Create){
							read(res.getFile().getName());
						}
					}
					
					try{Thread.sleep(1);}catch(InterruptedException e){}
					
					for(String str : new ArrayList<>(retry)){
						retry.remove(str);
						read(str);
					}
				}
			}
		};
	}
	
//------------------------------------------	
	
	private boolean read(final String filename){
		if(filename.startsWith(Main.PREFIX_NOT_READY)) return false;
		
		try{
			String channel = filename.substring(0, filename.lastIndexOf("'"));
			if(!Messenger.incomming.contains(channel)) return false;
			
			File f = new File(Main.CHANNEL_PATH, filename);
			if(!f.isFile()) return false;
			BufferedReader reader = new BufferedReader(new FileReader(f));
			ArrayList<String> messages = new ArrayList<>();
			String str;
			while((str = reader.readLine()) != null){
				messages.add(str);
			}
			reader.close();
			
			f.delete();
			
			PluginMessage msg = new PluginMessage(channel, messages);
			Main.pl.getProxy().getPluginManager().callEvent(new PluginMessageReceiveEvent(channel, msg));
		} catch (IndexOutOfBoundsException e){
			return false;
		} catch (IOException e) {
			retry.add(filename);
			return false;
		}
		return true;
	}
	
//------------------------------------------
	
	private void readExistingFiles(){
		for(File f : new File(Main.CHANNEL_PATH).listFiles()){
			if(!read(f.getName())){
				f.delete();
			}
		}
	}
}
