package de.xxlcreeper.bungee.pluginmessaging.pmc;

import java.util.ArrayList;

public class SplitPacket {

	private int noOfPackets;
	private ArrayList<String> parts;
	private String original;
	
	protected SplitPacket(int noOfPacekts, String originalChannel){
		this.noOfPackets = noOfPacekts;
		this.parts = new ArrayList<>();
		this.original = originalChannel;
	}

//------------------------------------------	
	
	protected void addPart(String part){
		this.parts.add(part);
		this.noOfPackets--;
	}
	
//------------------------------------------
	
	protected boolean isFinished(){
		return (this.noOfPackets == 0);
	}
	
	protected byte[] getContent(){
		String x = "";
		for(String str : parts){			
			x += str;
		}
		
		return x.getBytes();
	}
	
	protected String getOriginalChannel(){
		return this.original;
	}
	
}
