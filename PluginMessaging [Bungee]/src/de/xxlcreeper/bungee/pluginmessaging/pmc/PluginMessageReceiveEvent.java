package de.xxlcreeper.bungee.pluginmessaging.pmc;

import java.util.ArrayList;

import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.plugin.Event;

public class PluginMessageReceiveEvent extends Event{

	private PluginMessage message;
	
	public PluginMessageReceiveEvent(PluginMessage m){
		this.message = m;
	}
	
//------------------------------------------
	
	public PluginMessage message(){
		return this.message;
	}
	
	public Connection getSender(){
		return this.message.getSender();
	}
	
	public Connection getReceiver(){
		return this.message.getReceiver();
	}
	
	/**
	 * 
	 * @return Name of the Channel the Packet was sent in
	 */
	public String getChannel(){
		return this.message.getChannel();
	}
	
	/**
	 * 
	 * @return The original, no processed byte array
	 */
	public byte[] getMessageAsBytes(){
		return this.message.getMessageAsBytes();
	}
	/**
	 * 
	 * @return The original, no processed byte array as String
	 */
	public String getMessageAsString(){
		return this.message.getMessageAsString();
	}
	/**
	 * 
	 * @param i line
	 * @return The x. line of this PluginMessage
	 */
	public String getMessage(int i){
		return this.message.getMessage(i);
	}
	/**
	 * 
	 * @return ArrayList containing all Messages in this PluginMessage
	 */
	public ArrayList<String> getMessage(){
		return this.message.getMessage();
	}
}
