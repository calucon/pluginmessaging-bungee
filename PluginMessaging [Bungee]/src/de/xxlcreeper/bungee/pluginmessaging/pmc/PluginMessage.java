package de.xxlcreeper.bungee.pluginmessaging.pmc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.ServerConnection;
import net.md_5.bungee.api.connection.Connection;

public class PluginMessage {

	private String channel;
	private byte[] message;
	private Connection sender;
	private Connection receiver;
	private ArrayList<String> response;
	private boolean AWAIT = false;
	
	protected PluginMessage(String ch, byte[] msg, Connection sender, Connection receiver){
		this(ch, msg, sender, receiver, false);
	}
	
	protected PluginMessage(String ch, byte[] msg, Connection sender, Connection receiver, boolean isLocal){		
		this.channel = ch;
		this.message = msg;
		this.sender = sender;
		this.receiver = receiver;
		this.response = new ArrayList<>();
		
		if(isLocal){
			response = new ArrayList<>(Arrays.asList(new String(msg).split(Pattern.quote(PacketFactory.PATTERN_NEXT))));
		} else {
			try{
				ByteArrayDataInput in = ByteStreams.newDataInput(message);
				String str = "";
				while((str = in.readUTF()) != null){
					str = str.trim();
					if(str.equals(PluginMessageDebug.KEY_AWAIT)){
						AWAIT = true;
					} else {
						if(!str.equals("")){
							response.add(str);
						}
					}
				}	
			} catch(Exception e){}
			
			if(AWAIT == true){
				ArrayList<String> arr = new ArrayList<>(response);
				arr.add(0, channel);
				PluginMessageSender.send(arr, PluginMessageDebug.DEBUG_CHANNEL, ((ServerConnection)sender).getInfo());
			}	
		}
	}
	
//------------------------------------------
	
	public Connection getReceiver(){
		return this.receiver;
	}
	
	public Connection getSender(){
		return this.sender;
	}
	
	/**
	 * 
	 * @return Name of the Channel the Packet was sent in
	 */
	public String getChannel(){
		return this.channel;
	}
	
	/**
	 * 
	 * @return The original, no processed byte array
	 */
	public byte[] getMessageAsBytes(){
		return this.message;
	}
	/**
	 * 
	 * @return The original, no processed byte array as String
	 */
	public String getMessageAsString(){
		return new String(message);
	}
	/**
	 * 
	 * @param i line
	 * @return The x. line of this PluginMessage
	 */
	public String getMessage(int i){
		return this.response.get(i);
	}
	/**
	 * 
	 * @return ArrayList containing all Messages in this PluginMessage
	 */
	public ArrayList<String> getMessage(){
		return this.response;
	}
			
}
