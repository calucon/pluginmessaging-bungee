package de.xxlcreeper.bungee.pluginmessaging.pmc;

import net.md_5.bungee.api.plugin.Plugin;

public class Main{

	protected static Plugin pl;
	private boolean enabled = false;

	public Main(Plugin pl){
		if(!enabled){
			Main.pl = pl;
			new PluginMessageReceiver();
			PluginMessageReceiver.registerChannel(PluginMessageDebug.DEBUG_CHANNEL);
			PluginMessageReceiver.setNonUnregisterable(PluginMessageDebug.DEBUG_CHANNEL);
			PluginMessageReceiver.registerChannel(PacketFactory.SPLIT_PACKET_NOTIFY_CHANNEL);
			PluginMessageReceiver.setNonUnregisterable(PacketFactory.SPLIT_PACKET_NOTIFY_CHANNEL);
			PluginMessageReceiver.registerChannel(PacketFactory.SPLIT_PACKET_CHANNEL);
			PluginMessageReceiver.setNonUnregisterable(PacketFactory.SPLIT_PACKET_CHANNEL);	
			this.enabled = true;
		}
	}
	
}
