package de.xxlcreeper.bungee.pluginmessaging.pmc;

import java.util.ArrayList;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PluginMessageReceiver implements Listener{

	private static ArrayList<String> subscribed = new ArrayList<>();
	private static ArrayList<String> non_unregisterable =  new ArrayList<>();
	
	protected PluginMessageReceiver(){
		Main.pl.getProxy().getPluginManager().registerListener(Main.pl, this);
	}
	
//------------------------------------------	
	
	/**
	 * 
	 * @param channel
	 * 		The Plugin will start to listen for messages on the selected channel
	 */
	public static void registerChannel(String channel){
		if(!PluginMessageReceiver.subscribed.contains(channel)){
			ProxyServer.getInstance().registerChannel(channel);
			PluginMessageReceiver.subscribed.add(channel);	
			System.out.println("[PluginMessage] Listening to channel '" + channel + "'");
		}
	}
	
	/**
	 * 
	 * @param channel
	 * The Plugin will stop listening for messages on the selected channel
	 */
	public static void unregisterChannel(String channel){
		if(PluginMessageReceiver.subscribed.contains(channel) && !PluginMessageReceiver.non_unregisterable.contains(channel)){
			ProxyServer.getInstance().unregisterChannel(channel);
			PluginMessageReceiver.subscribed.remove(channel);
			System.out.println("[PluginMessage] Stopped listening to channel '" + channel + "'");
		}
	}
	
//------------------------------------------
	
	/**
	 * 
	 * @param channel
	 * Channel that can't be unregistered anymore until the Plugin stops
	 * @return unregisterable status set
	 */
	public static boolean setNonUnregisterable(String channel){
		if(PluginMessageReceiver.subscribed.contains(channel) && !PluginMessageReceiver.non_unregisterable.contains(channel)){
			return PluginMessageReceiver.non_unregisterable.add(channel);
		}
		return false;
	}
	
	/**
	 * 
	 * @return List of channels the Plugin is listening to
	 */
	public static ArrayList<String> getSubscribed(){
		return new ArrayList<>(PluginMessageReceiver.subscribed);
	}
	
//------------------------------------------
	
	@EventHandler
	public void onPluginMessage(PluginMessageEvent e){
		if(!PacketFactory.check(e.getTag(), e.getData(), e.getSender(), e.getReceiver())){
			PluginMessageDebug.check(e.getTag(), e.getData());
			if(PluginMessageReceiver.subscribed.contains(e.getTag())&& !e.getTag().equals(PluginMessageDebug.DEBUG_CHANNEL)){
				ProxyServer.getInstance().getPluginManager().callEvent(new PluginMessageReceiveEvent(
						new PluginMessage(e.getTag(), e.getData(), e.getSender(), e.getReceiver())));
			}	
		}
	}
	
}
