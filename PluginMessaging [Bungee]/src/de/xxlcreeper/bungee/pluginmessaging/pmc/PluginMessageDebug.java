package de.xxlcreeper.bungee.pluginmessaging.pmc;

import java.util.ArrayList;
import java.util.Collection;

public class PluginMessageDebug{

	public static final String DEBUG_CHANNEL = "PluginMessageDebug";
	private static boolean enabled = true;
	protected static String KEY_AWAIT = "DEBUG_AWAIT";
	protected static long delay = 1000;
	protected static ArrayList<PluginMessage> messages = new ArrayList<>();
	
	protected PluginMessageDebug(){}
	
//------------------------------------------
	
	protected static void check(String channel, byte[] message){
		if(channel.equals(PluginMessageDebug.DEBUG_CHANNEL) && PluginMessageDebug.isEnabled()){
			PluginMessage x = new PluginMessage(channel, message, null, null);
			
			for(PluginMessage msg : messages){	
				if(x.getMessage().size() == msg.getMessage().size()){
					boolean b = true;
					for(int i = 0; i < x.getMessage().size(); i++){
						if(!x.getMessage(i).equalsIgnoreCase(msg.getMessage(i))){
							b = false;
						}
					}
					if(b){
						PluginMessageDebug.messages.remove(msg);
						break;
					}
				}
			}
		}
		
	}
	
//------------------------------------------
	
	/**
	 * enables the Debugger (enabled by default)
	 */
	public static void enable(){
		PluginMessageDebug.enabled = true;
	}
	
	/**
	 * disables the Debugger
	 */
	public static void disable(){
		PluginMessageDebug.enabled = false;
	}
	
	/**
	 * 
	 * @return is the Debugger enabled
	 */
	public static boolean isEnabled(){
		return PluginMessageDebug.enabled;
	}
	
	/**
	 * 
	 * @param delay max time between sending the original Packet and receiving the "Debug" in milliseconds (1tick = 50ms)
	 */
	public static void setDelay(long delay){
		PluginMessageDebug.delay = delay;
	}
	
	/**
	 * 
	 * @return current delay of the Debugger
	 */
	public static long getDelay(){
		return PluginMessageDebug.delay;
	}
	
//------------------------------------------
	
	protected static ArrayList<String> addChannel(Collection<? extends String> coll, String channel){
		ArrayList<String> arr = new ArrayList<>(coll);
		arr.add(0, channel);
		return arr;
	}
	
	protected static ArrayList<String> getAwait(Collection<? extends String> coll){
		ArrayList<String> arr = new ArrayList<>(coll);
		arr.add(0, KEY_AWAIT);
		return arr;
	}
		
}
