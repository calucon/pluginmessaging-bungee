package de.xxlcreeper.bungee.pluginmessaging.pmc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.regex.Pattern;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Connection;

public class PacketFactory {
	protected static final int maxLength = 25600; //limit is 32776 but leaving some parts free for debug
	protected static final int baseLength = 24000;
	protected static final String SPLIT_PACKET_NOTIFY_CHANNEL = "SplitPacketNotifier";
	protected static final String SPLIT_PACKET_CHANNEL = "SplitPacketChannel";
//	protected static final String PATTERN_SPLIT = "[\\\"\"\\]";
	protected static final String PATTERN_NEXT = "[\\\"next\"\\]";
	private static HashMap<String, SplitPacket> split_receive_channels = new HashMap<>();
	
	protected static void sendSplitPackets(Collection<? extends String> messages, String channel, ServerInfo server){
		ArrayList<String> splits = getSplits(messages);
		
		PluginMessageSender.send(Arrays.asList(new String[]{String.valueOf(splits.hashCode()), String.valueOf(splits.size()), channel}), SPLIT_PACKET_NOTIFY_CHANNEL, server);
		for(String str : splits){
			PluginMessageSender.send(Arrays.asList(new String[]{String.valueOf(splits.hashCode()), str}), SPLIT_PACKET_CHANNEL, server);
		}
	}
	
//------------------------------------------
	
	protected static ArrayList<String> getSplits(Collection<? extends String> messages){
		String msg = listToString(messages);
		return splitInParts(msg, Math.round(msg.getBytes().length/baseLength));
	}
	
	private static ArrayList<String> splitInParts(String s, int parts){
		ArrayList<String> arr = new ArrayList<>();
		int length = Math.round(s.length()/parts);
		
		while(s.length() > length){
			String x = s.substring(0, length);
			s = s.substring(length);			
			arr.add(x);
		}
		arr.add(s);
		
		return arr;
	}
	
	private static String listToString(Collection<? extends String> messages){
		String x = "";
		for(String str : messages){
			x += PATTERN_NEXT + str;
		}
		x = x.replaceFirst(Pattern.quote(PATTERN_NEXT), "");
		return x;
	}
	
//------------------------------------------
	
	protected static boolean check(String channel, byte[] message, Connection sender, Connection receiver){
		if(channel.equals(PacketFactory.SPLIT_PACKET_NOTIFY_CHANNEL)){
			PluginMessage msg = new PluginMessage(channel, message, sender, receiver);
			split_receive_channels.put(msg.getMessage(0), new SplitPacket(Integer.parseInt(msg.getMessage(1)), msg.getMessage(2)));
			PluginMessageReceiver.registerChannel(msg.getMessage(0));
			return true;
		}
		if(channel.equals(SPLIT_PACKET_CHANNEL)){
			PluginMessage msg = new PluginMessage(channel, message, sender, receiver);
			if(split_receive_channels.containsKey(msg.getMessage(0))){
				SplitPacket packet = split_receive_channels.get(msg.getMessage(0));
				packet.addPart(msg.getMessage(1));
				
				if(packet.isFinished()){
					Main.pl.getProxy().getPluginManager().callEvent(new PluginMessageReceiveEvent(
					new PluginMessage(packet.getOriginalChannel(), packet.getContent(), sender, receiver, true)));
					split_receive_channels.remove(channel);
					PluginMessageReceiver.unregisterChannel(channel);
				}
			}
			return true;
		}
		
		return false;
	}
	
}
