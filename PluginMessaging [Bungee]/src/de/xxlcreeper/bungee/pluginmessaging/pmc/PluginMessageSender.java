package de.xxlcreeper.bungee.pluginmessaging.pmc;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

public class PluginMessageSender {

	/**
	 * 
	 * @param message The message
	 * @param channel The channel in which the message will be sent
	 * @param server The receiver of the Message
	 */
	public static void send(String message, String channel, String server){
		send(Arrays.asList(new String[]{message}), channel, server);
	}
	/**
	 * 
	 * @param message The message
	 * @param channel The channel in which the message will be sent
	 * @param server The receiver of the Message
	 */
	public static void send(String message, String channel, ServerInfo server){
		send(Arrays.asList(new String[]{message}), channel, server);
	}
	
	/**
	 * 
	 * @param messages List of Messages
	 * @param channel The channel in which the message will be sent
	 * @param server The receiver of the Message
	 */
	public static void send(Collection<? extends String> messages, String channel, String server){
		send(messages, channel, ProxyServer.getInstance().getServers().get(server));
	}
	/**
	 * 
	 * @param messages List of messages
	 * @param channel The channel in which the message will be sent
	 * @param server The receiver of the Message
	 */
	public static void send(Collection<? extends String> messages, final String channel, ServerInfo server){
		byte[] outgoing;
		
		if(PluginMessageDebug.isEnabled() && !channel.equals(PluginMessageDebug.DEBUG_CHANNEL)){
			byte[] debugPacket = packet(PluginMessageDebug.addChannel(messages, channel));
							/*packet(Arrays.asList(new String[]{channel, String.valueOf(messages.hashCode())}));*/
			outgoing = packet(PluginMessageDebug.getAwait(messages));
			
			if(pass(outgoing)) {
				server.sendData(channel, outgoing);
				final PluginMessage msg = new PluginMessage(PluginMessageDebug.DEBUG_CHANNEL, debugPacket, null, null);
				PluginMessageDebug.messages.add(msg);
				server.sendData(PluginMessageDebug.DEBUG_CHANNEL, debugPacket);
				
				new Timer().schedule(new TimerTask() {				
					@Override
					public void run() {
						if(PluginMessageDebug.messages.contains(msg)){
							System.err.println("[PluginMessage] The Bukkit Server probably did not receive your packet in the channel: '" + channel + "'. Make sure the Bungee is listening to that channel!");
							PluginMessageDebug.messages.remove(msg);
						}
					}
				}, PluginMessageDebug.delay);
			} else {
				PacketFactory.sendSplitPackets(messages, channel, server);
			}
		} else {
			outgoing = packet(messages);
			if(pass(outgoing)) {
				server.sendData(channel, outgoing);
			} else {
				PacketFactory.sendSplitPackets(messages, channel, server);
			}
		}
	}
	
//------------------------------------------
	
	private static boolean pass(byte[] outgoing){
		return (outgoing.length < PacketFactory.maxLength);
	}
	
	private static byte[] packet(Collection<? extends String> messages){
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bytes);
		try{
			for(String msg : messages){
				out.writeUTF(msg);
			}	
		} catch(IOException e){
			e.printStackTrace();
		}
		return bytes.toByteArray();
	}
	
}
